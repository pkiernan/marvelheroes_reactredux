import {SEARCH_FORM_UPDATE, SEARCH_RETURNED } from '../actions'

const defaultState = {
	string: '',
	type: 'character',
	results: []
}

const parseSearchResults = ({type, result}) => {
	try {
		return result.data.data.results.map(obj=> {
			let value;
			switch (type) {
				case 'creator':
					value = obj.fullName
					break;
				case 'character':
					value = obj.name
					break;
				case 'series':
				case 'comic':
				default:
					value = obj.title
					break;
			}
			return {
				id: obj.id,
				value
			}
		})
	} catch(e) {
		return [];
	}
}

const search_form = (state=defaultState, {type, payload}) => {
	switch (type) {
		case SEARCH_FORM_UPDATE:
			return {
				...state,
				[payload.key]: payload.value
			}
		case SEARCH_RETURNED:
			return {
				...state,
				results: parseSearchResults(payload)
			}
		default:
			return state
	}
}

export default search_form
