import { CONTENT_RETURNED, FETCH_CONTENT_BY_ID } from '../actions'

const defaultState = {
	isFetching: false,
	data: {}
}

const parseContentResult = ({type, result}) => {
	try {
		return result.data.data.results[0]
	} catch(e) {
		return {};
	}
}

const content = (state=defaultState, {type, payload}) => {
	switch (type) {
		case FETCH_CONTENT_BY_ID: 
			return {
				...state,
				isFetching: true
			}
		case CONTENT_RETURNED:
			return {
				...defaultState,
				data: parseContentResult(payload)
			}
		default:
			return state
	}
}

export default content
