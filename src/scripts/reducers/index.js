import { combineReducers } from 'redux'
import search_form from './search_form'
import content from './content'

const reducers = {
	search_form,
	content
}

export default reducers
