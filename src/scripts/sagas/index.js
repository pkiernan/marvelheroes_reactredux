import { call, put, takeEvery, takeLatest, select } from 'redux-saga/effects'
import * as api from '../api'
import { selectSearchType, selectSearchString } from '../selectors'
import { SEARCH_FORM_UPDATE, FETCH_CONTENT_BY_ID, searchReturned, contentReturned } from '../actions'

function* searchFormUpdate(action) {
	const type = yield select(selectSearchType);
	const string = yield select(selectSearchString);
	if (string === ' ' || !string.length) {
		yield put(searchReturned({type, result:{}}))
	} else {
		let data;
		switch (type) {
			case 'comic':
				data = yield call(api.getComicsByString, string);
				break
			case 'creator':
				data = yield call(api.getCreatorsByString, string);
				break
			case 'series':
				data = yield call(api.getSeriesByString, string);
				break
			case 'character':
			default:
				data = yield call(api.getCharactersByString, string);
				break
		}
		yield put(searchReturned({type,result: data}));
	}
}

function* fetchContent(action) {
	const {type, id} = action.payload
	let data;
	switch (type) {
		case 'comic':
			data = yield call(api.getComicByID, id);
			break
		case 'creator':
			data = yield call(api.getCreatorByID, id);
			break
		case 'series':
			data = yield call(api.getSeriesByID, id);
			break
		case 'character':
		default:
			data = yield call(api.getCharacterByID, id);
			break
	}	
	try {
		const results = data.data.data.results[0]
		results.images = results.images.filter(img=> results.thumbnail.path !== img.path)
	} catch(e) {
	}
	yield put(contentReturned({type,result: data}));
}

function* sagas() {	
	yield takeLatest(SEARCH_FORM_UPDATE, searchFormUpdate);
	yield takeLatest(FETCH_CONTENT_BY_ID, fetchContent);
}

export default sagas;