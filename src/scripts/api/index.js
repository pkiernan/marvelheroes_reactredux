import axios from "axios";
import {marvelapikey as apikey} from '../config_keys'

const base_uri = "https://gateway.marvel.com:443/v1/public/";
const limit = 5;

const callAPI = ({uri='', params={}}) => {
	return axios.get(`${base_uri}${uri}`, {
		params: {
			apikey,
			limit,
			...params
		}
	})
}

export const getComicsByString = (str) => callAPI({uri:'comics', params: {titleStartsWith:str} });
export const getCreatorsByString = (str) => callAPI({uri:'creators', params: {nameStartsWith:str} });
export const getSeriesByString = (str) => callAPI({uri:'series', params: {titleStartsWith:str} });
export const getCharactersByString = (str) => callAPI({uri:'characters', params: {nameStartsWith:str} });

export const getCharacterByID = (id) => callAPI({uri:`characters/${id}`});
export const getComicByID = (id) => callAPI({uri:`comics/${id}`});
export const getCreatorByID = (id) => callAPI({uri:`creators/${id}`});
export const getSeriesByID = (id) => callAPI({uri:`series/${id}`});
