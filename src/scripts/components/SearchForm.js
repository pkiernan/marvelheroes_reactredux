import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router'
import { updateSearchForm } from '../actions'
import { selectSearchType, selectSearchString, selectSearchResults } from '../selectors'

const getPlaceHolderByType = (type) => {
  switch (type) {
    case 'character':
      return 'e.g. Wolverine'
    case 'comic':
      return 'e.g. The Avengers'
    case 'creator':
      return 'e.g. Chris Claremont'
    case 'series':
      return 'e.g. X Men Omnibus'
    case 'default':
      return 'type search term here'
  }
}

const SearchFormResultsComponent = ({search_results, search_type}) => {
  return search_results.length? (
    <ul className='search_results'>
      {search_results.map(result=>{
        return (
          <li key={result.id}>
            <Link to={`${search_type}/${result.id}`}>{result.value}</Link>
          </li>
        )
      })}
    </ul>
  ) : null
}

const SearchFormComponent = (props) => {
    const {onChange, search_type, search_string, search_results} = props;
    return (
      <div className='search_area'>
        <form className='search_form'>
          <label>Search:</label>
          <select name='type' value={search_type} onChange={onChange} >
            <option value='character'>By Character</option>
            <option value='comic'>By Comic</option>
            <option value='creator'>By Creator</option>
            <option value='series'>By Series</option>
          </select>
          <input name='string' placeholder={getPlaceHolderByType(search_type)} value={search_string} onChange={onChange} autoFocus />
        </form>
        <SearchFormResultsComponent {...props} />
      </div>
    )
}


const mapStateToProps = (state) => {
  return {
    search_type: selectSearchType(state),
    search_string: selectSearchString(state),
    search_results: selectSearchResults(state),
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onChange: (e) => dispatch(updateSearchForm({key:e.target.name, value:e.target.value}))
  }
}

const SearchForm = connect(mapStateToProps, mapDispatchToProps)(SearchFormComponent)

export default SearchForm
