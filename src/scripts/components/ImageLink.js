import React from 'react'

const ImageLink = ({classnames,imageObject,caption='',linkurl=null,target='_self'}) => {
	const imgurl = `${imageObject.path}.${imageObject.extension}`
	if (linkurl === null) {
		linkurl = imgurl
		target = '_blank'
	}
	return (
		<a className={`image_link ${classnames})`} href={linkurl} target={target} >
			<img src={imgurl} alt={caption} />
		</a>
	)
}

export default ImageLink
