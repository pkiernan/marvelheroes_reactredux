import React from 'react'
import { Link } from 'react-router'
import ImageLink from './ImageLink'

const Icon = ({classname})=> {
	return (<i className={`icon fa ${classname}`} aria-hidden="true"></i>)
}

const getHeading = (type) => {
	switch (type) {
		case 'images':
			return 'Additional Images';
		case 'characters':
			return 'Featured Characters';
		case 'comics':
			return 'Featured Comics';
		case 'series':
			return 'Featured Series';
		case 'creators':
			return 'Featured Creators';
	}
}
const getRoute = (type) => {
	switch (type) {
		case 'characters':
			return 'character';
		case 'comics':
			return 'comic';
		case 'series':
			return 'series';
		case 'creators':
			return 'creator';
	}
}
const getIconClassName = (type) => {
	switch (type) {
		case 'characters':
			return 'fa-user';
		case 'comics':
			return 'fa-book';
		case 'series':
			return 'fa-files-o';
		case 'creators':
			return 'fa-pencil-square-o';
	}
}

const getTextLinksMap = (type) => ({name,resourceURI}) => {
	const id = resourceURI.substr(resourceURI.lastIndexOf('/') + 1, resourceURI.length-1);
	return <li key={id}>		
		<Link to={`${getRoute(type)}/${id}`}>
			<Icon classname={getIconClassName(type)} />
			<span>{name}</span>
		</Link>
	</li>
}

const getImageLinksMap = (type) => (imgObj, index) => {
	return <li key={index}><ImageLink imageObject={imgObj} /></li>
}

const AssociatedContentSection = ({list, type}) => {
	const heading = getHeading(type)
	let arr = null;
	if (list && list.items && list.items.length) {
		arr = list.items
	} else if (list && list.length) {
		arr = list
	}
	const typeClass = type==='images'? 'image_list' : 'text_list';
	return arr && arr.length? (
		<section className={`associated_list ${typeClass}`}>
			<h1>{heading}</h1>
			<ul>
				{arr.map(type==='images'?getImageLinksMap(type) : getTextLinksMap(type))}
			</ul>
		</section>
	) : null
}

export default AssociatedContentSection
