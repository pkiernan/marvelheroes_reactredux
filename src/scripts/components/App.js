import React from 'react'

class App extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <div id='main'>
        <header className='main_header'>
          <h1><a href='/'><strong>Marvel</strong>Studios</a></h1>
        </header>
        <div className='main_content'>
          {this.props.children}
        </div>
      </div>
    )
  }
}

export default App
