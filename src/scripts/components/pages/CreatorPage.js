import React from 'react'
import { connect } from 'react-redux'
import ImageLink from '../ImageLink'
import AssociatedContentSection from '../AssociatedContentSection'


class CreatorPage extends React.Component {

	componentDidMount() {
		const {fetchContent, asset_id} = this.props
		fetchContent(asset_id);
	}

	constructor(props) {
		super(props)
	}

	render() {
		const classnames = 'page creator_page';
		const {asset_id, isFetching, data} = this.props;
		const { id, fullName, firstName, lastName, thumbnail, series, comics } = data;
		return (isFetching || id===undefined)? (
			<section className={classnames}>
				<h1><i className="fa fa-circle-o-notch fa-spin fetchingIcon" aria-hidden="true"></i>Fetching...</h1>
			</section>
			) : (
			<section className={classnames}>
				<header className='title'>
					<h1>{fullName}</h1>
				</header>
				<article className='summary'>
					<ImageLink imageObject={thumbnail} caption={fullName} />
					<dl>
						<dt>First Name</dt>
						<dd>{firstName}</dd>
						<dt>Last Name</dt>
						<dd>{lastName}</dd>
					</dl>
				</article>
				<AssociatedContentSection list={comics} type={'comics'} />
				<AssociatedContentSection list={series} type={'series'} />
			</section>
		)
	}
}


export default CreatorPage
