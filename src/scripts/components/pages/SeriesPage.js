import React from 'react'
import { connect } from 'react-redux'
import ImageLink from '../ImageLink'
import AssociatedContentSection from '../AssociatedContentSection'


class SeriesPage extends React.Component {

	componentDidMount() {
		const {fetchContent, asset_id} = this.props
		fetchContent(asset_id);
	}

	constructor(props) {
		super(props)
	}

	render() {
		const classnames = 'page creator_page';
		const {asset_id, isFetching, data} = this.props;
		const { id, description, title, thumbnail, characters, comics, creators } = data;
		return (isFetching || id===undefined)? (
			<section className={classnames}>
				<h1><i className="fa fa-circle-o-notch fa-spin fetchingIcon" aria-hidden="true"></i>Fetching...</h1>
			</section>
			) : (
			<section className={classnames}>
				<header className='title'>
					<h1>{title}</h1>
				</header>
				<article className='summary'>
					<ImageLink imageObject={thumbnail} caption={title} />
					<dl>
						<dt>Title</dt>
						<dd>{title}</dd>
						<dt>Description</dt>
						<dd>{description && description.length? description : 'coming soon...'}</dd>
					</dl>
				</article>
				<AssociatedContentSection list={comics} type={'comics'} />
				<AssociatedContentSection list={characters} type={'characters'} />
				<AssociatedContentSection list={creators} type={'creators'} />
			</section>
		)
	}
}


export default SeriesPage
