import React from 'react'
import { connect } from 'react-redux'
import ImageLink from '../ImageLink'
import AssociatedContentSection from '../AssociatedContentSection'


class CharacterPage extends React.Component {

	componentDidMount() {
		const {fetchContent, asset_id} = this.props
		fetchContent(asset_id);
	}

	constructor(props) {
		super(props)
	}

	render() {
		const classnames = 'page character_page';
		const {asset_id, isFetching, data} = this.props;
		const { id, name, description, thumbnail, series, comics } = data;
		return (isFetching || id===undefined)?  (
			<section className={classnames}>
				<h1><i className="fa fa-circle-o-notch fa-spin fetchingIcon" aria-hidden="true"></i>Fetching...</h1>
			</section>) 
			: (
			<section className={classnames}>
				<header className='title'>
					<h1>{name}</h1>
				</header>
				<article className='summary'>
					<ImageLink imageObject={thumbnail} caption={name} />
					<dl>
						<dt>Name</dt>
						<dd>{name}</dd>
						<dt>Description</dt>
						<dd>{description && description.length? description : 'coming soon...'}</dd>
					</dl>
				</article>
				<AssociatedContentSection list={comics} type={'comics'} />
				<AssociatedContentSection list={series} type={'series'} />
			</section>
		)
	}
}


export default CharacterPage
