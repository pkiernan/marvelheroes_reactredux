import React from 'react'
import { connect } from 'react-redux'
import CharacterPage from './CharacterPage'
import CreatorPage from './CreatorPage'
import SeriesPage from './SeriesPage'
import ComicPage from './ComicPage'
import HomePage from './HomePage'
import { fetchContentByID } from '../../actions'
import { selectContentIsFetching, selectContentData } from '../../selectors'


const getMapStateToProps = () => (state) => {
	return {
		data: selectContentData(state),
		isFetching: selectContentIsFetching(state)
	}
}

const getMapDidpstchToProps = (type) => (dispatch) => {
	return {
		fetchContent: (id) => dispatch(fetchContentByID({type, id}))
	}
}


const PageFactory = (props) => {
	const {params} = props
	const page_id = params.page_id || '';
	const connectedContainer = 	connect(getMapStateToProps(), getMapDidpstchToProps(params.page_id))
	let Page;
	switch (params.page_id) {
		case 'character':
			Page = connectedContainer(CharacterPage)
			break;
		case 'comic':
			Page = connectedContainer(ComicPage)
			break;
		case 'series':
			Page = connectedContainer(SeriesPage)
			break;
		case 'creator':
			Page = connectedContainer(CreatorPage)
			break;
		default:
			Page = HomePage
			break;
	}
	return <Page {...params} />
}

export default PageFactory
