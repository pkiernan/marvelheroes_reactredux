import React from 'react'
import { connect } from 'react-redux'
import SearchForm from '../SearchForm'


const HomePageComponent = () => {
    return (
      <section className='home_page'>
        <h1>Welcome to the unofficial Marvel Studios search app</h1>
        <p>Please use the form below to search for a Marvel character, comic, creator or series</p>
        <SearchForm />
      </section>
    )
}


const mapStateToProps = (state) => {
  return {
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
  }
}

const HomePage = connect(mapStateToProps, mapDispatchToProps)(HomePageComponent)

export default HomePage
