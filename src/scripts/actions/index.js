export const SEARCH_FORM_UPDATE = 'SEARCH_FORM_UPDATE'
export const SEARCH_RETURNED= 'SEARCH_RETURNED'
export const FETCH_CONTENT_BY_ID = 'FETCH_CONTENT_BY_ID'
export const CONTENT_RETURNED= 'CONTENT_RETURNED'

export const updateSearchForm = (payload) => ({
	type: SEARCH_FORM_UPDATE,
	payload
})

export const searchReturned = (payload) => ({
	type: SEARCH_RETURNED,
	payload
})

export const fetchContentByID = (payload) => ({
	type: FETCH_CONTENT_BY_ID,
	payload
})

export const contentReturned = (payload) => ({
	type: CONTENT_RETURNED,
	payload
})
