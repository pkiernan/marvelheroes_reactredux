import { createSelector } from 'reselect'

const getSearchForm = ({search_form}) => search_form
const getContent = ({content}) => content

export const selectSearchType = createSelector([getSearchForm], ({type}) => type)
export const selectSearchString = createSelector([getSearchForm], ({string}) => string)
export const selectSearchResults = createSelector([getSearchForm], ({results}) => results)

export const selectContentIsFetching = createSelector([getContent], ({isFetching}) => isFetching)
export const selectContentData = createSelector([getContent], ({data}) => data)