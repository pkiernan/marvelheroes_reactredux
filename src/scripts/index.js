import '../styles/index.scss'

import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, combineReducers, applyMiddleware, compose } from 'redux'
import { Router, Route, IndexRoute, hashHistory } from 'react-router'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'
import createSagaMiddleware from 'redux-saga'

import reducers from './reducers'
import sagas from './sagas'
import App from './components/App'
import PageFactory from './components/pages/PageFactory'


const $app = document.getElementById('app')
const sagaMiddleware = createSagaMiddleware()
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
	combineReducers({
		...reducers,
		routing: routerReducer
	}),
	composeEnhancers(applyMiddleware(sagaMiddleware))
)

const history = syncHistoryWithStore(hashHistory, store)

sagaMiddleware.run(sagas)


ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRoute component={PageFactory} />
        <Route path=':page_id/:asset_id' component={PageFactory} />
      </Route>
    </Router>
  </Provider>,
  $app
)