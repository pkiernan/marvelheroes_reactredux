# Marvel Studios API Search #
Search heroes, stories, comics etc. from the marvel universe curtesy of the marvel api: developer.marvel.com

# Description #
From a drop down, select what category you wish to search (character, comic, creator, series) and input a search term in the input field. From the autofill menu offered, select the relevant result.
This will navigate to a new route depending on what your search category, with information on that result (e.g. 'wolverine': /character/1009718 )

# Technologies Used #
* React
* Redux
* React Router
* Sagas (redux-saga)
* Selectors (reselect)
* Webpack
* ES6/7
* HTML 5
* CSS3
* SASS

## Contributor
Paul Kiernan